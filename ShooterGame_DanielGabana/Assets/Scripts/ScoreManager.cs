﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static int score = 0; 

    Text text;

    // Restart when starting the game
    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }

    // Update score displayed
    void Update ()
    {
        text.text = "Score: " + score;
    }
}