﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private GameObject player;

    private NavMeshAgent navMesh;

    private Material DissolveMaterial;
    private Color initColorInside;

    // Set refferences to external Game Objects and components
    public void setRefs(GameObject plyer)
    {

        navMesh = GetComponent<NavMeshAgent>();
        DissolveMaterial = GetComponent<Renderer>().material;
        player = plyer;
        initColorInside = DissolveMaterial.GetColor("_ColorInside");
        DissolveMaterial.SetFloat("_DissolveSteps", 1.0f);
    }    

    // Start chasing the player
    void ChasePlayer()
    {
        if(navMesh.enabled)
            navMesh.SetDestination(player.transform.position);
    }

    void StopChasePlayer()
    {
        if(navMesh.enabled)
            navMesh.Stop();

    }

    public void Activate(float newspeed)
    {
        // Reset shader color to black
        DissolveMaterial.SetColor("_ColorInside", initColorInside);
        //DissolveMaterial.SetColor("_ColorInside", new Color(0.2f, 0.2f, 0.2f, 0));

        ChasePlayer();

        navMesh.speed = newspeed;

        // Dissolve in the enemy
        StartCoroutine(DissolveIn());
    }

    public void Deactivate()
    {
        StopChasePlayer();
        DissolveMaterial.SetColor("_ColorInside", new Color(0.4f, 0.3f, 0.2f, 0));

        // Dissolve out the enemy
        StartCoroutine(DissolveOut());

        
    }

    ////////////////// ANIMATION ////////////////////////////

    IEnumerator DissolveIn()
    {
        float currentStep = DissolveMaterial.GetFloat("_DissolveSteps");

        // This would be executed every frame untill the enemy has displayed completely
        do{
            currentStep -= 0.02f;
            DissolveMaterial.SetFloat("_DissolveSteps", currentStep);

            // Wait until next frame.
            yield return null;
        }
        while (currentStep > 0);
    }

    IEnumerator DissolveOut()
    {
        float currentStep = DissolveMaterial.GetFloat("_DissolveSteps");

        do{
            currentStep += 0.01f;
            DissolveMaterial.SetFloat("_DissolveSteps", currentStep);

            // Wait until next frame.
            yield return null;
        }
        while (currentStep < 1);

        transform.gameObject.SetActive(false);
    }

    ////////////////// INTERACTION ////////////////////////////


    void OnTriggerEnter (Collider col)
    {
        // if enemy gets to hit the player
        if(col.gameObject.name == "AreaPlayerDamage")
        {
            player.GetComponent<PlayerController>().DamageHealth();
            Deactivate();
        }
    }

    // When enemy is shot, destroy it and increase Score
    public void Hitted(){
    	Deactivate();

        ScoreManager.score += 10;
    }
}
