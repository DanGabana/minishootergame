﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public float maxTimeSpawn = 4.0f;
    public float minTimeSpawn = 2.0f;
    public float enemiesSpeed = 3.0f;
    public EnemiesPool pool;


	private Vector3 spawnPosition;

    public void StartGame()
    {
        pool.createPool();

        // Start spawning enemies after 1 second
        Invoke("SpawnEnemy", 1);
    }

    // Restart game after it is over
    public void RestartGame()
    {
        maxTimeSpawn = 4.0f;
        minTimeSpawn = 2.0f;

        enemiesSpeed = 3.0f;
        // Start spawning enemies after 1 second
        Invoke("SpawnEnemy", 1);
    }

    void SpawnEnemy()
    {
    	CancelInvoke(); // Stop the timer, just in case

    	// different radius for X and Z, to make it elliptical
    	float randX = transform.position.x + (Random.insideUnitSphere.x * 20);
    	float randZ = transform.position.z + (Random.insideUnitSphere.z * 5);
    	
    	// ignore Y position
    	Vector3 finalPos = new Vector3(randX, -0.29f, randZ);

        // Get enemy from pool to activate
        GameObject enemyFromPool = pool.GetEnemyFromPool();
        // position and rotation
        enemyFromPool.transform.position = finalPos;
    	enemyFromPool.transform.rotation = Quaternion.identity;
        // The game object has to be active before doing the dissolve
        enemyFromPool.SetActive(true);
        // Activate so it starts moving
        enemyFromPool.GetComponent<Enemy>().Activate(enemiesSpeed);

    	// call again this function to spawn another enemy
    	Invoke("SpawnEnemy", Random.Range (minTimeSpawn, maxTimeSpawn));
    }

    // Stop spawining enemies and deactivate all the currently active ones
    public void StopSpawn()
    {
        CancelInvoke();
        pool.DeactivateAllEnemies();
    }
}
