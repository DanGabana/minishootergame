﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesPool : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private GameObject player;

    [SerializeField]
    private int poolSize = 30; // Number of enemies created when starting the game

	Queue<GameObject> queueEnemies = new Queue<GameObject>();

    // Create a pool of enemies to get, instead of instantiating new ones
    public void createPool()
    {
    	for(int i = 0; i< poolSize; i++)
    	{
    		GameObject obj = Instantiate(enemy);
            
            obj.SetActive(false);
            // The enemy needs to know where the player is to attack him/her
            obj.GetComponent<Enemy>().setRefs(player);
            queueEnemies.Enqueue(obj);
    	}
    }

    // Get enemy to activate on scene
    public GameObject GetEnemyFromPool()
    {
        // take first item in the queue
        GameObject enemyToSpawn = queueEnemies.Dequeue();

        // put it back in the queue to re-use it
        queueEnemies.Enqueue(enemyToSpawn);

        return enemyToSpawn;
    }

    // When the game is over
    public void DeactivateAllEnemies()
    {
        // Deactivate the enemies currently active
        foreach (GameObject go in queueEnemies)
        {
            if(go.activeSelf){
                go.GetComponent<Enemy>().Deactivate();
            }
        }
    }
}
