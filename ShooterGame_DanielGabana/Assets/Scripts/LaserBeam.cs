﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeam : MonoBehaviour
{
    [SerializeField]
    private GvrReticlePointer reticlePointer;
    [SerializeField]
    private LineRenderer lineRenderer;

    Vector3 currentLengthLine;

   public void UpdateBeam () {
    
        // Get the ray from the ReticlePointer and transform from global to local coordinates
        Vector3 destLength = transform.InverseTransformPoint(GvrReticlePointer.CalculateRay(reticlePointer, reticlePointer.raycastMode).ray.GetPoint(reticlePointer.ReticleDistanceInMeters));

        // for some reason, the reticle starts at (0, 0, -0.1) before it starts interacting with object
        if(destLength.z < 0)
        {
            // force to Max Distance
            destLength = new Vector3(destLength.x, destLength.y, 20);   
        }

        if(currentLengthLine.z < destLength.z){
            currentLengthLine = new Vector3(destLength.x, destLength.y, currentLengthLine.z + 0.8f); 
        }else{
            // make it the same size directly (to prevent the line staying long when player is holding the trigger and moving around)
            currentLengthLine = destLength; 
        }


        // Inidicate the line renderer the position (mainly Z axis, which is distance)
        // substract 0.9 since the beam is a bit ahead the Reticle Point in the scene
        lineRenderer.SetPosition(1, new Vector3(currentLengthLine.x, currentLengthLine.y, currentLengthLine.z - 0.9f));
    }

    public void Reset()
    {
        // Since this method is calles by PlayerController before activating the object, the first time this is false 
        // Surely there's a better way to do this tho!
        if (!transform.gameObject.activeInHierarchy)
        {
            // Reset the line to 0 to make it grow
            currentLengthLine = new Vector3();
        }
    }
}
