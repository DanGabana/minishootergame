﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
	float targetTime;
	float timeLeft;
	bool act = false;
	Action functionCallback;

    // Set Up references and target time of this timer
    public void SetUp(float targTime, Action functToCall)
    {
        targetTime = targTime;
        timeLeft = targetTime;
        act = true;
        functionCallback = functToCall;
    }

    // Update is called once per frame
    public void UpdateTimer()
    {
    	if(act)
    	{
	        timeLeft -= Time.deltaTime;
            // Then timer is over
		    if(timeLeft <= 0)
		    {
		    	TimerEnded();
		    }
		}
    }

    public void StopTimer()
    {
    	act = false;
    }

    // Do something when timer is over
    void TimerEnded()
    {
        functionCallback();
        // to re-start the timer
        timeLeft = targetTime;
    }
}
