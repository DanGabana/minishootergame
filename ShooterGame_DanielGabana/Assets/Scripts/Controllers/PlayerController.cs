﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour 
{
    public LaserBeam LaserBeam;

    [SerializeField]
    private MainController mainCtrl;
    [SerializeField]
    private Image imgHealthBar; 
    [SerializeField]
    private SpriteRenderer circleAreaPlayerRenderer;
    [SerializeField]
    private GvrReticlePointer reticlePointer;

	float currentHealth = 1.0f;
	float targetHealth = 1.0f;

	bool showDamage = false;
	Timer timerSpawnEnemies = new Timer();

	// Reset values when restarting the game
	public void ResetGame()
	{
		currentHealth = 1.0f;
		targetHealth = 1.0f;
		imgHealthBar.fillAmount = 1;
		ScoreManager.score = 0;
	}

    // Update is called once per frame
    void Update()
    {
    	// Control the player's health (heart)
    	if(targetHealth < currentHealth && !mainCtrl.EndGame){
        	imgHealthBar.fillAmount = imgHealthBar.fillAmount -0.005f;
        	currentHealth = imgHealthBar.fillAmount;

        	// If heart is empty, the game is over
        	if(imgHealthBar.fillAmount == 0)
        	{
        		mainCtrl.GameOver();
        	}
    	}

    	// Start timer to put the circle area back to its original color
    	if(showDamage)
    		timerSpawnEnemies.UpdateTimer();

    	// When user is clicking (trigger). Would have to test on Cardboard to check whether it is Trigger or Touch
    	if(reticlePointer.Triggering){
    		ShowLaserBeam();
    	}else{
    		HideLaserBeam();
    	}
    }

    public void DamageHealth()
    {
    	targetHealth -= 0.2f;
    	// Turn circle red for a bit to indicate that damage has been done
    	circleAreaPlayerRenderer.material.color = new Color(1, 0, 0);
    	// Make timer to put the player's circle area back to black
    	timerSpawnEnemies.SetUp(0.3f, ResetCircleArea);

    	showDamage = true;
    }

    // Reset circle to back as well as its timer
    public void ResetCircleArea()
    {
    	timerSpawnEnemies.StopTimer();

		circleAreaPlayerRenderer.material.color = new Color(0, 0, 0);
		showDamage = false;
    }

    ////////////////////////////////////////// SHOW & HIDE LASER

    public void ShowLaserBeam()
    {
        LaserBeam.Reset();
    	LaserBeam.gameObject.SetActive(true);
    }

    public void HideLaserBeam()
    {
    	LaserBeam.gameObject.SetActive(false);
    }

}


