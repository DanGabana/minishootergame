﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public UIIntro introUI;
	public UIGameOver gameOverUI;

	/////////////////////////////////////////////////////////////////////// INTRO UI

    public IEnumerator ShowIntroUI()
	{
		yield return StartCoroutine(introUI.introFader.FadeIn());
	}

	public IEnumerator HideIntroUI()
	{
		yield return StartCoroutine(introUI.introFader.FadeOut());
		introUI.transform.gameObject.SetActive(false);
	}
	
	/////////////////////////////////////////////////////////////////////// GAME OVER UI

	public IEnumerator ShowGameOverUI()
	{
		gameOverUI.transform.gameObject.SetActive(true);
		yield return StartCoroutine(gameOverUI.gameOverFader.FadeIn());
	}

	public IEnumerator HideGameOverUI()
	{
		yield return StartCoroutine(gameOverUI.gameOverFader.FadeOut());
		gameOverUI.transform.gameObject.SetActive(false);
		gameOverUI.RestartButton();
	}
}
