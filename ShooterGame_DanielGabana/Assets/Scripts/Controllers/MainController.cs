﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
	public bool EndGame = false;

    [SerializeField]
	private EnemySpawner enemySpawner;
    [SerializeField]
    private UIController uiCtrl;
    [SerializeField]
    private PlayerController playerCtrl;

	Timer timerSpeeds = new Timer();
	Timer timerSpawnEnemies = new Timer();

	float timeSpeeds = 20.0f; // Speeds change every 20 seconds
	float timeWaves = 10.0f; // Waves of enemies increase every 10 seconds
	

    // Update is called once per frame
    void Update()
    {
	    timerSpeeds.UpdateTimer();
	    timerSpawnEnemies.UpdateTimer();

        if(!EndGame && uiCtrl.introUI.buttonUI.gameObject.active)
            uiCtrl.introUI.buttonUI.UpdateButton();
        else if (EndGame)
            uiCtrl.gameOverUI.buttonUI.UpdateButton();

        playerCtrl.LaserBeam.UpdateBeam();
    }

    public void StartGame()
    {
    	// Hide Intro UI
    	StartCoroutine (uiCtrl.HideIntroUI ());

    	// Set up and activate timers
    	timerSpeeds.SetUp(timeSpeeds, ChangeEnemiesSpeed);
    	timerSpawnEnemies.SetUp(timeWaves, ChangeSpawnTime);

    	// Start spawning enemies!
    	enemySpawner.StartGame();
    }    

    // Restart game when it is over
    public void RestartGame()
    {
    	// Hide Game Over UI
    	StartCoroutine (uiCtrl.HideGameOverUI ());

    	EndGame = false;
    	playerCtrl.ResetGame();

    	// Set up and activate timers
    	timerSpeeds.SetUp(timeSpeeds, ChangeEnemiesSpeed);
    	timerSpawnEnemies.SetUp(timeWaves, ChangeSpawnTime);

    	// Start spawning enemies!
    	enemySpawner.RestartGame();
    }  

    // When game finishes
    public void GameOver()
    {
    	EndGame = true;
    	// Stop timers and spawning enemies
    	timerSpeeds.StopTimer();
    	timerSpawnEnemies.StopTimer();
    	enemySpawner.StopSpawn();

    	// Show Game Over UI
    	StartCoroutine (uiCtrl.ShowGameOverUI ());
    }

    //////////////////// CHANGE ENEMIES BEHAVIOUR ///////////////

    void ChangeEnemiesSpeed()
    {
    	// increase speed by 1 every X seconds
    	float newSpeed = enemySpawner.enemiesSpeed + 0.5f;
    	enemySpawner.enemiesSpeed = newSpeed;
    }

    void ChangeSpawnTime()
    {
    	// Make spawn time faster!
    	
    	if(enemySpawner.maxTimeSpawn > 1.0f){
    		enemySpawner.maxTimeSpawn = enemySpawner.maxTimeSpawn - 1.0f;
    	}

    	if(enemySpawner.minTimeSpawn > 0.5f){
    		enemySpawner.minTimeSpawn = enemySpawner.minTimeSpawn - 0.5f;
    	}
    }
    
}
