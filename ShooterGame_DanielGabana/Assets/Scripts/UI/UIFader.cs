﻿using System;
using UnityEngine;
using System.Collections;

public class UIFader : MonoBehaviour
{
    [SerializeField]
    private float fadeSpeed = 1.0f;      
    [SerializeField]
    private CanvasGroup groupToFade; 

    // Fade in CanvasGoup
    public IEnumerator FadeIn()
    {
        float lowestAlpha;

        // This would be executed every frame untill the UI has displayed completely
        do
        {
            lowestAlpha = 1f;

            // Fade in alpha
            groupToFade.alpha += fadeSpeed * Time.deltaTime;

            // Update value of variable to exit this function once the animation is completed
            if (groupToFade.alpha < lowestAlpha)
                lowestAlpha = groupToFade.alpha;

            // Wait until next frame.
            yield return null;
        }
        while (lowestAlpha < 1f);
    }

    ////////////////////////////////////////////////////////////////////////////////

    public IEnumerator FadeOut ()
    {
        float highestAlpha;

        // This would be executed every frame untill the UI has faded completely
        do
        {
            highestAlpha = 0f;

            // Fade out alpha
            groupToFade.alpha -= fadeSpeed * Time.deltaTime;

            // Update value of variable to exit this function once the animation is completed
            if (groupToFade.alpha > highestAlpha)
                highestAlpha = groupToFade.alpha;

            // Wait until next frame.
            yield return null;
        }
        while (highestAlpha > 0f);
    }
}
