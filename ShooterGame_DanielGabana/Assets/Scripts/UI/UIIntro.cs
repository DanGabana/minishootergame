﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIIntro : MonoBehaviour
{
    [SerializeField]
	private MainController mainCtrl;

    public UIButton buttonUI;

    public UIFader introFader;

    void Start()
    {

        buttonUI.SetUp(StartGame);
    }

    // Start game for the first time
	public void StartGame()
    {
        mainCtrl.StartGame();
    }
}
