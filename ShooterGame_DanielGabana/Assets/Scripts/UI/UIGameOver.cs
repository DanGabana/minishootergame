﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameOver : MonoBehaviour
{
    [SerializeField]
    private MainController mainCtrl;

    public UIButton buttonUI;

    public UIFader gameOverFader;

    void Start()
    {
        buttonUI.SetUp(RestartGame);
    }

    // To restart game once it is over
	public void RestartGame()
    {
        mainCtrl.RestartGame();
    }

    // Restart button, in another function as it is done only when the button is completely hidden
    public void RestartButton()
    {
        buttonUI.Restart();
    }
}
