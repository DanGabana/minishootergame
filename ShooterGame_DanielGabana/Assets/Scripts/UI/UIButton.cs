﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour
{
   	//public GameObject callbackGO;
    [SerializeField]
	private Image imgToFill;

	Action functionCallback;

    bool mouseOver = false;
	bool mouseClick = false;
	bool act = true;

	float fillSpeed = 0.02f;

    // Update is called once per frame
    public void UpdateButton()
    {
    	// To fill or clear the button's bar, only when player is holding the button
        if(mouseOver && mouseClick && act){
        	imgToFill.fillAmount = imgToFill.fillAmount + fillSpeed;

        	// When button is completelly filled, do something
        	if(imgToFill.fillAmount == 1)
        	{
        		act = false;
                functionCallback();
            }

    	}else if(!mouseClick && imgToFill.fillAmount > 0 && act){
    		imgToFill.fillAmount = imgToFill.fillAmount - fillSpeed;
    	}
    }

	// Set up Button: GameObject to call once the timer is completed, and the function to call
	public void SetUp(Action functToCall)
    {
        act = true;
        functionCallback = functToCall;
    }

    // Reset button when restarting the gam
    public void Restart()
    {
    	mouseOver = false;
		mouseClick = false;
		act = true;
		imgToFill.fillAmount = 0;
    }


    /////////////////////////////////////////////// BUTTON AND MOUSE INTERACTION HANDLERS (WITH RETICLE)

    public void MouseEnter()
    {
        mouseOver = true;
    }

    public void MouseExit()
    {
        mouseOver = false;
        mouseClick = false;
    }

    public void MouseDown()
    {
        mouseClick = true;
    }

    public void MouseUp()
    {
        mouseClick = false;
    }
}
