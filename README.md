# MiniShooterGame

Made by Daniel Gabana - 20/03/2019

This is a prototype shooting game for CardboardVR.


:: HOW TO PLAY ::

The game is very simple. You just have to shoot at the enemies (spheres) coming at you. 

If the enemy hits you, you would loose some health, displayed in a heart shape on the top right corner. The enemy will hit you if it gets into your area, displayed with a circle around you.

Note: You can shoot anywhere anytime, but to kill the enemies, you have to click on them, not just 'touch' them with the laser beam... otherwise it would be too easy!


:: HOW TO RUN THE GAME LOCALLY ::

You would need to use Unity 2019.2 (or any newer version) to open this game. 

You would have to install the Lightweight Render Pipeline to make the shaders work. You can follow this simple tutorial to install it: https://www.youtube.com/watch?v=bhzSy0T2oC4 (watch from minute 2:01)

If you have any question or problem, please do not hesitate to contact me.

Before running the game, you may have to change the Build Settings (File > Build Settings) to the Android platform. Just select it from the list and click 'Switch Platform'. Once the platform has been changed, go to 'Player Settings' and make sure the Minimum Api Level, under 'Other Settings', is set to Android 4.4 'KitKat'. Then, you are good to build the project and test it in your Cardboard!

You are all done, just click the Play button and enjoy the game!



 :: TODO ::

The game could be improved in many ways. For example, enemies could be killed when the laser beam gets to hit them, and not when the trigger is being pressed (as it is now).
Also, the enemies could have a life bar on top of them showing their health when they are being hit. This would add some difficulty to the game as the player would have to hold the trigger to kill the enemies, instead of using just one shot.